# "Dependency Injection" example
Well, this is not real dependency injection, but a try to use python features to achieve similar goals (in a
yet quite primitive way).

The "services" module is the "context" where the services are instantiated and held. By calling the setup function,
a certain set of dependencies are prepared. At the moment this is done statically (with 3 options) but it could also
be done using naming conventions or likewise.

```some_module``` is the actual top-level implementation module. ```some_module_test``` illustrates the overriding
of services for unit tests.

Note that the concept is not limited to classes - whole modules an be replaced.

However, compared to spring, this lacks support for profiles, hierarchical configurations, dependency resolution, etc.