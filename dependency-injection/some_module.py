import services as svc

class MyCode(object):
    def work(self):
        svc.sleep_service.sleep(5)
        svc.print_service.print_lines('test', 10)
        svc.sleep_service.sleep(5)
