import sys
import services
from some_module import MyCode

if len(sys.argv) > 1 and sys.argv[1] == 'pro':
    services.setup('pro')
elif len(sys.argv) > 1 and sys.argv[1] == 'wonderland':
    services.setup('wonderland')
else:
    services.setup('dev')


module = MyCode()
module.work()