import time

print("PRO sleep service initialized!")
    
def sleep(seconds):
    print("sleeping %d seconds ..." % seconds)
    time.sleep(seconds)
    print("waking up!")
