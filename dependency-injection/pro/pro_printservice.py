import services as svc

print("PRO printing service initialized!")
    
def print_lines(what, howmany=1):
    for i in range(0, howmany):
        print("%d: %s" % (i, what))
        svc.sleep_service.sleep(1)
