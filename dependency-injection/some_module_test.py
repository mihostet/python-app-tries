from unittest.mock import Mock
import pytest

@pytest.fixture
def svc():
    import services as svc
    svc.sleep_service = Mock(
        sleep = Mock(return_value=None)
    )
    svc.print_service = Mock(
        print_lines = Mock(return_value=None)
    )
    return svc

@pytest.fixture
def code():
    from some_module import MyCode 
    return MyCode()
    
def test(code, svc):
    code.work()
    svc.print_service.print_lines.assert_called_once_with('test', 10)