import sys

sleep_service = None
print_service = None

def setup(env):
    global sleep_service, print_service
    if env == 'pro':
        from pro import pro_sleepservice, pro_printservice
        sleep_service = pro_sleepservice
        print_service = pro_printservice
    elif env == 'dev':
        from dev import dev_sleepservice, dev_printservice
        sleep_service = dev_sleepservice
        print_service = dev_printservice
    elif env == 'wonderland':
        from pro import pro_printservice
        from dev import dev_sleepservice
        sleep_service = dev_sleepservice
        print_service = pro_printservice
    else:
        raise ImportError("env is not properly set: %s (supported: pro, dev, wonderland)" % env)