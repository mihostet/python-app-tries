# Application using PyQt5 and Plotting
This is a very simple application using PyQt5 as UI, embedding Matplotlib and pyqtgraph for plotting.
The UI is built in Qt Designer, and includes placeholder widgets for the Matplotlib and pyqtgraph widgets, respectively.

Before running, compile the UI using pyuic5:
```
pyuic5 somegui.ui > somegui_ui.py
python main.py
```