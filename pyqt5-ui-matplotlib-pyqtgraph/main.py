import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from PyQt5.QtCore import QObject, pyqtSlot, QTimer
from somegui_ui import Ui_MainWindow
import numpy as np

import pyqtgraph as pg
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
# pg.setConfigOption('leftButtonPan', False)


class TestAppMain(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.x = np.full((10000,),np.nan)
        self.y = np.full((10000,),np.nan)
        self.t = 0
        self.setupUi(self)
        self.qplot_plot = self.qplot_widget.plot(self.x, self.y, pen=pg.mkPen('k', width=2))
        timer = QTimer(self)
        timer.timeout.connect(self.makepoint)
        timer.start(50)
    
    def makepoint(self):
        self.t += self.in_freq.value()
        self.add_point(self.t, np.sin(self.t/10)*self.in_ampl.value()+self.in_offset.value())
        
    def add_point(self, x, y):
        self.x[:-1] = self.x[1:]
        self.y[:-1] = self.y[1:]
        self.x[-1] = x
        self.y[-1] = y
        self.plot()
    
    def plot(self):
        self.qplot_plot.setData(self.x, self.y)
        self.plot_widget.axes.cla()
        self.plot_widget.axes.plot(self.x,self.y)
        self.plot_widget.draw()


app = QApplication(sys.argv)

root = TestAppMain()
root.show()

sys.exit(app.exec_())